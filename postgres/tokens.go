package postgres

import (
	"github.com/jmoiron/sqlx"
	"encoding/json"
	"io"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

const TokenServiceSchema = `
CREATE TABLE IF NOT EXISTS tokens (
  id BIGSERIAL PRIMARY KEY,
  token varchar(255),
  user_id int8 REFERENCES users (id),
  created timestamp
);
`

// Ensure TokenService implements latencyAt.TokenService.
var _ latencyAt.TokenService = &TokenService{}

type TokenService struct {
	db                 *sqlx.DB
	stmtSelectByToken  *sqlx.Stmt
	stmtSelectByUserID *sqlx.Stmt
	stmtInsert         *sqlx.Stmt
	stmtDelete         *sqlx.Stmt
	stmtDeleteAll      *sqlx.Stmt
}

func NewTokenService(db *sqlx.DB) (*TokenService, error) {
	ts := &TokenService{db: db}
	for ref, str := range map[**sqlx.Stmt]string{
		&ts.stmtSelectByToken:  "SELECT id, token, user_id, created FROM tokens WHERE token = $1",
		&ts.stmtSelectByUserID: "SELECT id, token, created FROM tokens WHERE user_id = $1",
		&ts.stmtDelete:         "DELETE FROM tokens WHERE id = $1 AND user_id = $2",
		&ts.stmtInsert:         "INSERT INTO tokens (token, user_id, created) VALUES($1, $2, $3) RETURNING id",
		&ts.stmtDeleteAll:      "DELETE FROM tokens",
	} {
		s, err := db.Preparex(str)
		if err != nil {
			return nil, err
		}
		*ref = s
	}
	return ts, nil
}

func (s *TokenService) Healthy() error {
	return s.db.Ping()
}

func (s *TokenService) TokensByUserID(id int) ([]latencyAt.Token, error) {
	rows, err := s.stmtSelectByUserID.Query(id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	tokens := []latencyAt.Token{}
	for rows.Next() {
		if err := rows.Err(); err != nil {
			return tokens, err
		}
		token := latencyAt.Token{UserID: id}
		if err := rows.Scan(&token.ID, &token.Token, &token.Created); err != nil {
			return tokens, err
		}
		tokens = append(tokens, token)
	}
	return tokens, nil
}

func (s *TokenService) TokenByToken(token string) (*latencyAt.Token, error) {
	rows, err := s.stmtSelectByToken.Query(token)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if !rows.Next() {
		return nil, errors.ErrTokenNotFound
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	t := &latencyAt.Token{}
	return t, rows.Scan(&t.ID, &t.Token, &t.UserID, &t.Created)
}

func (s *TokenService) TokenFromJSON(r io.Reader) (*latencyAt.Token, error) {
	token := latencyAt.Token{}
	return &token, json.NewDecoder(r).Decode(&token)
}

// Create inserts a new user into the database
func (s *TokenService) CreateToken(u *latencyAt.Token) error {
	return s.stmtInsert.QueryRow(
		u.Token,
		u.UserID,
		u.Created,
	).Scan(&u.ID)
}

func (s *TokenService) DeleteToken(u *latencyAt.Token) error {
	result, err := s.stmtDelete.Exec(u.ID, u.UserID)
	if err != nil {
		return err
	}
	n, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if n == 0 {
		return errors.ErrTokenNotFound
	}
	return err
}

func (s *TokenService) Purge() error {
	_, err := s.stmtDeleteAll.Exec()
	return err
}
