package http

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

type TokenAuth struct {
	JWTSigningMethod jwt.SigningMethod
	JWTSigningKey    []byte
}

type HandlerErrFuncPrivate func(r *http.Request, ps httprouter.Params, id int) (interface{}, error)

// RequireToken returns a new handler which only gets executed when JWT can be validated.
func (t *TokenAuth) Require(h HandlerErrFuncPrivate, requireActivation bool) HandlerErrFunc {
	return func(r *http.Request, ps httprouter.Params) (interface{}, error) {
		id, activated, err := t.validateToken(r.Header.Get("Authorization"))
		if err != nil {
			return nil, &errors.Error{Err: errors.ErrJWTInvalid, StatusCode: http.StatusForbidden}
		}
		if requireActivation && !activated {
			return nil, &errors.Error{Err: errors.ErrAccountNotActivated, StatusCode: http.StatusForbidden}
		}
		return h(r, ps, id)
	}
}

func (t *TokenAuth) validateToken(authHeader string) (int, bool, error) {
	fields := strings.Fields(authHeader)
	if len(fields) != 2 || fields[0] != "Bearer" {
		return 0, false, fmt.Errorf("Header %s invalid", authHeader)
	}
	token, err := jwt.Parse(fields[1], func(token *jwt.Token) (interface{}, error) {
		if token.Method != t.JWTSigningMethod {
			return nil, errors.ErrJWTInvalidSigningMethod
		}
		return t.JWTSigningKey, nil
	})
	if err != nil {
		return 0, false, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return 0, false, errors.ErrJWTInvalidClaim
	}
	if !token.Valid {
		return 0, false, errors.ErrJWTInvalid
	}
	exp, ok := claims["exp"].(float64) // Why float?!
	if !ok {
		return 0, false, fmt.Errorf("Token %v not int64", exp)
	}
	if int64(exp) < time.Now().Unix() {
		return 0, false, fmt.Errorf("Token expired %f", exp)
	}
	id, ok := claims["user_id"].(float64)
	if !ok {
		return 0, false, errors.ErrJWTInvalidUID
	}
	activated, ok := claims["activated"].(bool)
	if !ok {
		return 0, false, errors.ErrJWTInvalidActivated
	}
	return int(id), activated, nil
}

func (t *TokenAuth) NewToken(uid int, activated bool) (*latencyAt.JWT, error) {
	token := jwt.NewWithClaims(t.JWTSigningMethod, jwt.MapClaims{
		"user_id":   uid,
		"activated": activated,
		"exp":       time.Now().Add(time.Hour * 24).Unix(),
	})
	stoken, err := token.SignedString(t.JWTSigningKey)
	if err != nil {
		return nil, err
	}
	return &latencyAt.JWT{Token: stoken}, nil
}
