package latencyAt

import (
	"io"
	"time"
)

type Token struct {
	ID      int       `json:"id"`
	Token   string    `json:"token"`
	UserID  int       `json:"-"`
	Created time.Time `json:"created"`
}

type TokenService interface {
	Healthy() error

	TokenByToken(token string) (*Token, error)
	TokensByUserID(id int) ([]Token, error)
	TokenFromJSON(r io.Reader) (*Token, error)
	CreateToken(u *Token) error
	DeleteToken(u *Token) error
	Purge() error
}
