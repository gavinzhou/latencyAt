package mock

import (
	"io"

	"gitlab.com/latency.at/latencyAt"
)

type TokenService struct {
}

func (s *TokenService) TokenByToken(token string) (*latencyAt.Token, error) {
	panic("not implemented")
}

func (s *TokenService) TokensByUserID(id int) ([]latencyAt.Token, error) {
	panic("not implemented")
}

func (s *TokenService) TokenFromJSON(r io.Reader) (*latencyAt.Token, error) {
	panic("not implemented")
}

func (s *TokenService) CreateToken(u *latencyAt.Token) error {
	panic("not implemented")
}

func (s *TokenService) DeleteToken(u *latencyAt.Token) error {
	panic("not implemented")
}

func (s *TokenService) Purge() error {
	panic("not implemented")
}

func (s *TokenService) Healthy() error {
	panic("not implemented")
}
